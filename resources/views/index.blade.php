<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans|VT323" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
</head>
<body>
    <div class="wrapper">
<div class="content">
<h1>Text Analyzer App</h1>
    <h2>{{$errors->first('inputString')}}</h2>
    <form action="{{route('result')}}" method="post">
        <label class="label" for="inputString">Enter Text:</label><br>
        <textarea class="text" name="inputString" id="" cols="30" rows="10" placeholder="Start typing here ..."></textarea><br>
        <input type="submit" class="btn">
        {{csrf_field()}}
    </form>
    </div>
    </div>
</body>
</html>