<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans|VT323" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">

</head>
<body>
<div class="wrapper">
<div class="resultContent">
    <h1>Text Analyzer App</h1>
    <h3>You entered:</h3>
    <h2>{{ $string }}</h2>
    <table border="1">
        <thead>
            <td>Character</td>
            <td>Ocurrencies</td>
            <td>Before</td>
            <td>After</td>
            <td>Maximum distance</td>
        </thead>
        <tbody>
            @foreach($result as $value)
            <tr>
                <td>{{$value[0]}}</td>
                <td>{{$value[1]}}</td>
                <td>{{$value[2]}}</td>
                <td>{{$value[3]}}</td>
                <td>{{$value[4]}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>

</body>
</html>