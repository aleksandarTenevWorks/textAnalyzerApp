<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FormValidation;


class MyAppController extends Controller
{
    public function index()
    {
        return view('index');
    }
    public function result(FormValidation $request)
    {
        
        $postData = $request->validated();
        $postDataArray = str_split(strtolower($postData['inputString']));
        $string = $postData['inputString'];
        $result = [];
        foreach(array_count_values($postDataArray) as $key=>$value)
        {
            $next = '';
            $prev = '';
            $previousItemKey = null;
            if($key != ' ')
                {
                    $itemDiffArray = [];
                    for($i = 0 ; $i < count($postDataArray) ; $i++)
                        {
                            if($key == $postDataArray[$i])
                            {
                                $diff = $i - $previousItemKey;
                                array_push($itemDiffArray , $diff);
                                $previousItemKey = $i;

                                if($i < (count($postDataArray) - 1))
                                {
                                    $next .= $postDataArray[$i + 1] . ',' ;
                                } 
                                if ($i > 0)
                                {
                                    $prev .= $postDataArray[$i - 1] . ',';
                                }
                            }
                            
                        }
                    $itemDiff = null;
                    if(count($itemDiffArray) >= 2)
                    {
                        unset($itemDiffArray[0]);
                        $itemDiff = max($itemDiffArray) -1;
                    }
                    $arr = array_keys($postDataArray, $key);
                    $row = [$key, $value,trim($next,','), trim($prev,',') , $itemDiff];
                    array_push($result,$row);
                }

        }
        
        return view('result' , compact('result' , 'string' , 'itemDiff' ));
    }
    
}
